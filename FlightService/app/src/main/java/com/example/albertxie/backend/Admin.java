package com.example.albertxie.backend;

/**
 * Object representing a new Administrator object.
 */
public class Admin extends User {

	private static final long serialVersionUID = 4080613403114628167L;
	
	/**
	 * Initialize a new Admin with given parameters.
	 * @param email the email of the Admin
	 * @param password the password of the Admin
	 */
	public Admin(String password, String email) {
		super(password, email);
	}
}