package com.example.albertxie.backend;

import java.util.Comparator;

/**
 * ItineraryCostComparator object that compares the cost of Itinerary.
 */
public class ItineraryCostComparator implements Comparator<Itinerary> {
	
	/**
	 * Returns an integer indicating which Itinerary object is greater.
	 * @param i1  the first Itinerary object to be compared
	 * @param i2  the second Itinerary object to be compared
	 * @return an integer indicating which Itinerary object is greater
	 */
	public int compare(Itinerary i1, Itinerary i2) {
		return (int) (i1.getTotalCost() - i2.getTotalCost());
	}

}