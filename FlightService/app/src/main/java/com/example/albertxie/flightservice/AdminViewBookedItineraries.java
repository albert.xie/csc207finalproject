package com.example.albertxie.flightservice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

import com.example.albertxie.backend.Admin;
import com.example.albertxie.backend.Client;
import com.example.albertxie.backend.FlightServiceManager;
import com.example.albertxie.backend.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity to provide a list of clients where admin can then select one to
 * view their booked itineraries
 */
public class AdminViewBookedItineraries extends AppCompatActivity {



    private ArrayList<Client> clientList = new ArrayList<Client>();
    private Client client;

    private FlightServiceManager fsm;
    private User user;
    public static final String FSM_KEY = "FSM_KEY";
    public static final String USER_DATA_KEY ="USER_DATA";

    @Override
    /**
     * Initialize activity, unpack intent, and create list of clients to be
     * viewed in list view
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_view_booked_itineraries);

        Intent intent = getIntent();
        fsm = (FlightServiceManager) intent
                .getSerializableExtra(LoginActivity.FSM_KEY);
        user = (User) intent.getSerializableExtra(LoginActivity.USER_DATA_KEY);


        buildClientList();

        String[] clientNames = new String[clientList.size()];

        int n = 0;
        while (n < clientNames.length){
            clientNames[n] = clientList.get(n).getEmail();
            n += 1;
        }

        //for each client, add client to array
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, clientNames);
        ListView listView = (ListView) findViewById(R.id.admin_options_list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(mMessageClickedHandler);
        Log.d("debug trace 1", "admin view created");
    }

    /**
     * Adapterview to listen for an item to be selected from list view
     */
    public AdapterView.OnItemClickListener mMessageClickedHandler = new
            AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView parent, View view, int position
                , long id) {
            client = clientList.get(position);
            editActivity(client);
        }
    };

    /**
     * Method used to create a list of only clients from the list of all users
     */
    private void buildClientList(){
        //ArrayList<User> tempList = new ArrayList<User>();
        ArrayList<User> tempList = fsm.getUserList();

        for (User x : tempList) {
            if (!isAdmin(x)) { // current user is not admin
                // add this user to the array list
                this.clientList.add((Client) x);
            }
        }
    }

    /**
     * Check if given user is an Admin class
     * @param user given user being checked
     * @return true if user is admin, false otherwise
     */
    private boolean isAdmin(User user) {
        return user.getClass().equals(Admin.class);
    }


    public void editActivity(Client client){
        Intent loginIntent;

        loginIntent = new Intent(this, ClientViewBookedItineraries.class);
        loginIntent.putExtra(FSM_KEY, fsm);
        loginIntent.putExtra(USER_DATA_KEY, client);
        loginIntent.putExtra("FROM_CLIENT", false);
        loginIntent.putExtra("ADMIN_DATA", user);

        startActivity(loginIntent);
    }
}
