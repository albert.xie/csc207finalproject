package com.example.albertxie.backend;

import android.util.Log;

import com.example.albertxie.flightservice.LoginActivity;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.io.IOException;

/**
 * Object that manages users, itineraries, and flights of a
 * flight booking application.
 */
public class FlightServiceManager implements Serializable {

	private static final long serialVersionUID = 3157321907749632244L;

	private Database<Flight> flightData;
	private Database<User> userData;

	private String userFilePath, flightFilePath;

	/**
	 * Instantiates an instance of FlightServiceManager.
	 */
	public FlightServiceManager() {
		this.flightData = new Database<Flight>();
		this.userData = new Database<User>();
	}

    /**
     * Instantiates an instance of FlightServiceManager.
     * @param userDataFile Serial file where the users database is stored.
     * @param flightDataFile Serial file where the flights are stored.
     */
    public FlightServiceManager(File userDataFile, File flightDataFile) {
		flightData = new Database<Flight>();
		userData = new Database<User>();

        // Loading user data
		if(userDataFile.exists()) {
            loadUserData(userDataFile.getPath());
        }
        else {
            try {
                userDataFile.createNewFile();
				this.userData = new Database<User>();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // Loading flight data
        if(flightDataFile.exists()) {
            loadFlightData(flightDataFile.getPath());
        }
        else {
            try {
                flightDataFile.createNewFile();
				this.flightData = new Database<Flight>();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        this.userFilePath = userDataFile.getPath();
        this.flightFilePath = flightDataFile.getPath();

    }

    public void addUser(User u) {
		if(this.getUserByEmail(u.getEmail()) == null) {
			this.userData.add(u);
		}
		else if (u.getClass().equals(Client.class)) {

			Client userAlias = (Client) this.getUserByEmail(u.getEmail());

			userAlias.setLastName(((Client) u).getLastName());
			userAlias.setFirstName(((Client) u).getFirstName());
			userAlias.setAddress(((Client) u).getAddress());

			userAlias.setCreditCardNumber(((Client) u).getCreditCardNumber());
			userAlias.setExpiryDate(((Client) u).getExpiryDate());
			userAlias.setPassword(u.getPassword());
		}
    }

    public void addFlight(Flight f) {
		if(this.getFlightByFlightNumber(f.getFlightNumber()) == null) {
			this.flightData.add(f);
		}
		else {
			Flight alias = this.getFlightByFlightNumber(f.getFlightNumber());
			alias.editFlight(
					f.getFlightNumber(),
					f.getDepartureDate(),
					f.getArrivalDate(),
					f.getAirline(),
					f.getOrigin(),
					f.getDestination(),
					f.getCost()
			);
		}
    }

	public Flight getFlightByFlightNumber(String flightNumber) {
		Flight result = null;
		for(Flight f : this.getFlightList()) {
			if(f.getFlightNumber().equals(flightNumber)) {
				result = f;
			}
		}
		return result;
	}

	/**
	 * Writes given object to file at given path location.
	 * @param o Object to be written to file
	 * @param path path Object is to be written to.
	 */
	private void writeSerial(Object o, String path) {
		try {
			OutputStream file = new FileOutputStream(path);
			OutputStream buffer = new BufferedOutputStream(file);
			ObjectOutput output = new ObjectOutputStream(buffer);
			output.writeObject(o);
			output.close();
		} catch(IOException e) {
            Log.d("FSM", "IOException at writeSerial()");
			e.printStackTrace();
		}
	}



	/**
	 * Returns an object containing data read from SER file.
	 * @param path path to ser file.
	 * @return Object containing data from SER file.
	 * @throws ClassNotFoundException
	 */
	private Object readSerial(String path) throws ClassNotFoundException {
        Object returnObject = null;

        try {
            InputStream file = new FileInputStream(path);
            InputStream buffer = new BufferedInputStream(file);
            ObjectInput input = new ObjectInputStream(buffer);

            returnObject = input.readObject();
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
		return returnObject;
	}


	public void loadFlightData(String path) {

		try {
			InputStream file = new FileInputStream(path);
			InputStream buffer = new BufferedInputStream(file);
			ObjectInput input = new ObjectInputStream(buffer);

			this.flightData = (Database<Flight>) input.readObject();
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch(ClassNotFoundException e) {
			this.flightData = new Database<Flight>();
		}
	}
	
	/**
	 * Creates userData database with data from CSV file. Database holds User
	 * objects.
	 */
	public void loadUserData(String path) {

		try {
			InputStream file = new FileInputStream(path);
			InputStream buffer = new BufferedInputStream(file);
			ObjectInput input = new ObjectInputStream(buffer);

			this.userData = (Database<User>) input.readObject();
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Writes flightData database to file.
	 */
	public void saveFlightData() {
		this.writeSerial(this.flightData, flightFilePath);
	}
	
	/**
	 * Writes userData database to file.
	 */
	public void saveUserData() {
		this.writeSerial(this.userData, userFilePath);
	}
	
    /**
     * Returns and ArrayList containing data read from CSV file. Each index
     * in the ArrayList contains one line from the CSV file.
     * @param path path to the CSV file.
     * @return ArrayList containing data from CSV file.
     */
    public ArrayList<String[]> readFromCSV(String path) {
        String[] line;
        ArrayList<String[]> returnValue = new ArrayList<String[]>();
        
        try {
            Scanner scanner = new Scanner(new FileInputStream(path));
            while(scanner.hasNext()) {
                line = scanner.nextLine().split(",");
                
                for(int x = 0; x < line.length; x++) {
                    line[x] = line[x].trim();
                }
                
                returnValue.add(line);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        
        return returnValue;
    }
	
	/**
	 * Reads a new flight from CSV file and adds it to flightData.
	 * @param path path to the CSV file.
	 */
	public void uploadFlightFromCSV(String path) {
		ArrayList<String[]> strings = this.readFromCSV(path);

		for(String[] i : strings) {

			Log.d("test_csv_load", i[0]);

			Flight uploadedFlight = new Flight(i[0], i[1], i[2], i[3], i[4],
					i[5], Double.parseDouble(i[6]), Integer.parseInt(i[7]));

			this.addFlight(uploadedFlight);
		}
	}
	
	/**
	 * Reads a new user from CSV file and adds to user to userData database.
	 * @param path path to the CSV file
	 */
	public void uploadUserFromCSV(String path) {
		ArrayList<String[]> strings = this.readFromCSV(path);
		for(String[] i : strings) {

			User uploadedUser;
			System.out.println(i);
			if(i[7].equals("a")) {

				uploadedUser = new Admin(i[6], i[2]);
			}
			else {
				uploadedUser =
						new Client(i[0], i[1], i[2], i[3], i[4], i[5], i[6]);
			}
			this.addUser(uploadedUser);
		}
	}

	/**
	 * Returns an ArrayList of the User objects stored.
	 * @return an ArrayList of the User objects stored
	 */
	public ArrayList<User> getUserList(){
		return this.userData.getData();
	}

	/**
	 * Returns an ArrayList of Flight objects stored.
	 * @return an ArrayList of Flight objects stored
	 */
	public ArrayList<Flight> getFlightList(){
		return this.flightData.getData();
	}

    /**
     * Returns the User object with the given username.
     * @param email username of a User object
     * @return the User object with the given username
     */
    public User getUserByEmail(String email){
        for (User x : this.getUserList()){
            if (x.getEmail().equals(email)){
                return x;
            }
        }
        return null;
    }
	
	/**
	 * Returns a list of flight objects that depart from given departure city
	 * after the given departure time but on the same day and lands in given
	 * destination city.
	 * @param origin departure city for flight object
	 * @param destination destination city for flight object
	 * @param date Date and time of departure. Format is YYYY-MM-D HH:MM
	 * @return List of Flight objects that meet criteria
	 */
	public List<Flight> searchFlight(String date, String origin, 
			String destination) {
		// searching for certain flight inside data
		// create list of all flights objects from flight database
		List<Flight> flightList =
				new ArrayList<Flight>(this.flightData.getData());
		
		List<Flight> returnList = new ArrayList<Flight>();
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date departureDateParsed = new Date();
		
		try {
			departureDateParsed = format.parse(date + " 00:00");
		} catch(ParseException e) {
			e.printStackTrace();
		}
		
		// loop through each object in list until you find matching flight.
		for(Flight f : flightList) {
			if((f.getOrigin().equals(origin)) && 
					(f.getDestination().equals(destination)) && 
					(f.getDepartureTime().getTime() >= 
					departureDateParsed.getTime()) && f.haveSpace()) {
				returnList.add(f);
			}
		}
		return returnList;
	}

	/**
	 * Returns an ArrayList of Itinerary objects, that satisfies the given
	 * origin, departure time and destination.
	 * @param origin origin of the first Flight
	 * @param departureTime  departure time of the first Flight, with the 
	 * 						 format YYYY-MM-D HH:MM
	 * @param destination	 destination of the final Flight
	 * @return a ArrayList of Itinerary that satisfies the given origin, 
	 * 		   departure time and destination
	 */
	public ArrayList<Itinerary> getItinerary(String origin, 
			String departureTime, String destination){
		
		// 1. create a ArrayList of Itineraries to be returned as 
		// function terminate
		ArrayList<Itinerary> itinerariesList = new ArrayList<Itinerary>();
		// 2. loop the Flight objects find the ones with the correct origin 
		// and departure time
		for (Flight x: this.flightData.getData()){
			// 3. create a new Itinerary with the initial Flight, and add it to
			// itineraries
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			Date departureDateParsed = new Date();
			
			try {
				departureDateParsed = format.parse(departureTime);
			} catch(ParseException e) {
				e.printStackTrace();
			}
			
			if ((x.getOrigin().equals(origin)) &&
					(x.getDepartureTime().getTime() >=
					departureDateParsed.getTime())) {
				Itinerary itinerary = new Itinerary();
				itinerary.addFlight(x);
				itinerariesList.add(itinerary);
			}
			
		}
		ArrayList<String> visited = new ArrayList<String>();
		visited.add(origin);
		// 5. this will gives an ArrayList of all possible Flight paths, 
		// with no loop,but not guaranteeing it will arrive to the destination.

		ArrayList<Itinerary> allPossiblePaths = 
				generatePossiblePaths(destination, visited, itinerariesList);
		ArrayList<Itinerary> correctPaths = new ArrayList<Itinerary>();
		// 6. loop through all the paths, since Itinerary is a LinkedList, 
		// we can simply find if the last Flight object has the same 
		// destination as the destination.
		for (Itinerary i : allPossiblePaths){
			if (i.getLast().getDestination().equals(destination)) {
				boolean haveSeat = true;
				for (Flight f: i.getFlights()){
					if (!f.haveSpace()){
						haveSeat = false;
					}
				} if (haveSeat){
					correctPaths.add(i);
				}
			}
		}
		return correctPaths;
	}
	
	/**
	 * Returns an ArrayList of possible flight paths, including flight paths
	 * that do not lead to the correct destination.
	 * @param destination the destination of the flight path, this acts like a
	 * 		  checker for the base case
	 * @param visited	  ArrayList of Flight objects that has been visited in
	 * 					  the flight paths
	 * @param itineraries ArrayList of Itinerary objects that needs to be 
	 * 					  extended until destination has been reached or ran 
	 * 					  out of Flight that has not been visited
	 * @return an ArrayList of possible flight paths, including flight paths
	 * 		   that do not lead to the correct destination.
	 */
	private ArrayList<Itinerary> generatePossiblePaths(String destination, 
			ArrayList<String> visited, ArrayList<Itinerary> itineraries) {
		
		ArrayList<Itinerary> result = new ArrayList<Itinerary>();
		for (Itinerary i : itineraries){
			// if Itinerary has one Flight and its a direct flight, halt 
			// searching for additional paths for this Itinerary
			if (i.getLast().getDestination().equals(destination)){
				result.add(i);
			} else{
				for (Flight f : this.flightData.getData()){
				// find Flights that: 1 has not visited before, 2 flights are
				// connected, 3 within 6 hours
					if (!visited.contains(f.getDestination()) && (f.getOrigin().equals
							(i.getLast().getDestination())) && 
							(withinTimeFrame(i.getLast(), f))){
						// if the Flight doesn't go to destination directly...
						if (!f.getDestination().equals(destination)){
							// creates a copy of i
							Itinerary clone = new Itinerary();
							// for here you have to do i.getFlights 
							// instead of i
							for (Flight x : i.getFlights()) {
								clone.addFlight(x);
							}
							clone.addFlight(f);
							ArrayList<Itinerary> newList =
									new ArrayList<Itinerary>();
							newList.add(clone);
							// adds the recursive ArrayList to result
							ArrayList<String> newVisited =
									new ArrayList<String>();
							for (String y: visited){
								newVisited.add(y);
							}
							newVisited.add(f.getDestination());
							result.addAll(generatePossiblePaths(destination,
									newVisited, newList));
					// if Flight is direct/ base case
					} 	
						else {
							// creates a copy of i
							Itinerary clone = new Itinerary();
							for (Flight x : i.getFlights()) {
								clone.addFlight(x);
							}	
							clone.addFlight(f);
							result.add(clone);
						}
					}
				}
			}
		}
		return result;
	}
	
	/**
	 * Returns true if flight1 and flight2 are within 6 hour, false otherwise.
	 * @param flight1	the Flight that just arrived
	 * @param flight2	the Flight that is about to depart
	 * @return true if flight1 and flight2 are within 6 hour, false otherwise
	 */
	public boolean withinTimeFrame(Flight flight1, Flight flight2) {
		return (flight1.getArrivalTime().before(flight2.getDepartureTime())
				&& flight2.getDepartureTime().getTime() < 
				flight1.getArrivalTime().getTime() + 
				Constants.TIME_FRAME_HOURS * Constants.MILLISECONDS_IN_HOUR);
	}	
	
	/**
	 * Returns a ArrayList of Itinerary sorted by cost from lowest to highest.
	 * @param itineraries	the ArrayList of Itinerary to be sorted
	 * @return a ArrayList of Itinerary sorted by cost from lowest to highest
	 */
	public ArrayList<Itinerary> sortByCost(ArrayList<Itinerary> itineraries){
		Collections.sort(itineraries, new ItineraryCostComparator());
		return itineraries;
	}
	
	/**
	 * Returns a ArrayList of Itinerary sorted by time
	 * from shortest to longest.
	 * @param itineraries	the ArrayList of Itinerary to be sorted
	 * @return a ArrayList of Itinerary sorted by time from shortest
	 * to longest
	 */
	public ArrayList<Itinerary> sortByTime(ArrayList<Itinerary> itineraries) {
		Collections.sort(itineraries, new ItineraryTimeComparator());
		return itineraries;
	}

    /**
     * Returns a string of a list of searched flights with one flight
     * per line.
     * @param searchedFlights a list of searched flights
     * @return the flights that depart from origin and arrive at destination
	 * on the given date formatted with one flight per line.
     */
    public String printSearchedFlights(List<Flight> searchedFlights){
        String returnVal = "";

        for(Flight f : searchedFlights) {
            returnVal+= f.toString() + "\n";
        }
        return returnVal.trim();
    }
	
	/**
	 * Returns a string of a given list of itineraries.
	 * @param itineraries an array list of itineraries, could be 
	 * 		  sorted/ unsorted.
	 * @return itineraries that depart from origin and arrive at destination
	 * 		   on the given date with lay-overs at or under 6 hours. Where each
	 *         line contains one flight, followed by total cost and total time
	 *         each on their own line.
	 */
	public String printItineraries(ArrayList<Itinerary> itineraries){
		String returnVal = "";
		for(Itinerary i : itineraries) {
			returnVal += i.toString() + "\n";
		}
		return returnVal.trim();
	}

    /**
     * Returns a string representation of the flight data stored.
     * @return a string representation of the flight data stored
     */
    public String printFlightData(){
        String flights = "";
        for (Flight x: this.flightData.getData()){
            flights += x.toString() + "\n";
        }
        return flights.substring(0, flights.length()-1);
    }

    /**
     * Returns string representation of the user data stored.
     * @return String representation of userData
     */
    public String printUserData(){
        String users = "";
        for (User x: this.userData.getData()){
            users += x.toString() + "\n";
        }
        return users.substring(0, users.length() - 1);
    }

}
