package com.example.albertxie.backend;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Database object that stores objects in an ArrayList.
 * Database can be serialized.
 */
public class Database<T> implements Serializable {
	
	private static final long serialVersionUID = -3742515301517378351L;

	// ArrayList of the contained data
	private ArrayList<T> data;
	
	/**
	 * Initializes a empty Database with the given type.
	 */
	public Database() {
		this.data = new ArrayList<T>();
	}
	
	/**
	 * Initializes a Database populated with Collection of Objects.
	 * @param data	Collection of Object with type T
	 */
	public Database(Collection<T> data) {
		this.data = new ArrayList<T>(data);
	}
	
	/**
	 * Adds an Object to the Database.
	 * @param object  the Object to be added to the Database
	 */
	public void add(T object) {
		this.data.add(object);
	}
	
	/**
	 * Returns the data stored in its original ArrayList<T> form.
	 * @return the data stored in its original ArrayList<T> form
	 */
	public ArrayList<T> getData() {
		return this.data;
	}
}
