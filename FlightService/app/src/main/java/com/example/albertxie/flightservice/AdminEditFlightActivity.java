package com.example.albertxie.flightservice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.albertxie.backend.Flight;
import com.example.albertxie.backend.FlightServiceManager;
import com.example.albertxie.backend.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Edit information for a given flight passed through intent
 */
public class AdminEditFlightActivity extends AppCompatActivity {

    FlightServiceManager fsm;
    Flight flight;
    Flight aliasingFlight;
    User user;

    String flightNumber;
    String departureTime;
    String arrivalTime;
    String airline;
    String origin;
    String destination;
    double cost;

    /**
     * Initialize activity, unpack intent and populate the flight fields
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_edit_flight);

        Intent i = getIntent();

        fsm = (FlightServiceManager) i.getSerializableExtra(LoginActivity
                .FSM_KEY);
        Log.d("aef", "before loading flight");
        flight = (Flight) i.getSerializableExtra("FLIGHT_KEY");
        Log.d("aef", "after loading flight");

        user = (User) i.getSerializableExtra(LoginActivity.USER_DATA_KEY);

        // set displayFlightField to show the current editing flight info
        TextView displayFlightField = (TextView) findViewById(R.id
                .displayFlightField);

        Log.d("aef", flight.getAirline());

        displayFlightField.setText(flight.toString());

        Log.d("aef", "display the shit flight");



        // since aliasing doesn't apply for flight, so we have to manually set
        // flight to point to the correct flight in fsm.
        for (Flight f: fsm.getFlightList()){
            Log.d("aef", f.toString());


            // since by definition of flight number on assignment, flight
            // number is unique.
            if (f.getFlightNumber().equals(flight.getFlightNumber())){

                aliasingFlight = f;
                Toast toast = Toast.makeText(getApplicationContext(),
                        aliasingFlight.toString(), Toast.LENGTH_LONG);
                toast.show();
            }
        }
        Toast toast = Toast.makeText(getApplicationContext(),
                aliasingFlight.toString(), Toast.LENGTH_LONG);
        toast.show();
    }

    /**
     * If EditText fields have been changed then apply those changes to their
     * respective fields in given flight
     * @param view defautl parameter
     */
    public void applyChange(View view) {

        EditText flightNumberField = (EditText) findViewById
                (R.id.flightNumberField);
        EditText departureTimeField = (EditText) findViewById
                (R.id.departureTimeField);
        EditText arrivalTimeField = (EditText) findViewById
                (R.id.arrivalTimeField);
        EditText airlineField = (EditText) findViewById
                (R.id.airlineField);
        EditText originField = (EditText) findViewById(R.id.originField);
        EditText destinationField = (EditText) findViewById
                (R.id.destinationField);
        EditText costField = (EditText) findViewById(R.id.costField);

        this.flightNumber = flightNumberField.getText().toString();

        Toast toast = Toast.makeText(getApplicationContext(),
                flightNumber, Toast.LENGTH_SHORT);
        toast.show();

        this.departureTime = departureTimeField.getText().toString();
        this.arrivalTime = arrivalTimeField.getText().toString();
        this.airline = airlineField.getText().toString();
        this.origin = originField.getText().toString();
        this.destination = destinationField.getText().toString();

        if (costField.getText().toString().equals("")){
            this.cost = 0.00;
        }
        else {
            this.cost = Double.parseDouble(costField.getText().toString());
        }

        if (this.flightNumber.equals("")) {
            this.flightNumber = flight.getFlightNumber();
        }
        if (this.airline.equals("")) {
            this.airline = flight.getAirline();
        }
        if (this.origin.equals("")) {
            this.origin = flight.getOrigin();
        }
        if (this.destination.equals("")) {
            this.destination = flight.getDestination();
        }
        if (this.cost == 0.00){
            this.cost = flight.getCost();
        }
        // destination and arrival time is been handled
        // in editFlight, if it is not empty.
        aliasingFlight.editFlight(this.flightNumber,
                this.departureTime, this.arrivalTime,
                this.airline, this.origin, this.destination, this.cost);
        Toast a = Toast.makeText(getApplicationContext(),
                aliasingFlight.toString(), Toast.LENGTH_SHORT);
        a.show();
        fsm.saveFlightData();

        Intent intent = new Intent(this, AdminActivity.class);
        intent.putExtra(LoginActivity.FSM_KEY, fsm);
        intent.putExtra(LoginActivity.USER_DATA_KEY, user);
        startActivity(intent);
    }


}
