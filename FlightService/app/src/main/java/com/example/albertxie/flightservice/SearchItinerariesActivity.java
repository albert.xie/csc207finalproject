package com.example.albertxie.flightservice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.albertxie.backend.Admin;
import com.example.albertxie.backend.Client;
import com.example.albertxie.backend.Flight;
import com.example.albertxie.backend.FlightServiceManager;
import com.example.albertxie.backend.Itinerary;
import com.example.albertxie.backend.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity that searches for Itinerary with given input. If an admin is using this activity, it has
 * the ability to add an Itinerary to the selected User. If client is using this activity, it can
 * add the selected Itinerary to itself.
 */
public class SearchItinerariesActivity extends AppCompatActivity {

    FlightServiceManager fsm;
    User user;
    ArrayList<Itinerary> itineraryList;
    ArrayList<Client> clientList;
    Itinerary selectedItinerary;
    private boolean fromClient;

    /**
     * Initialises this activity.
     * @param savedInstanceState the Bundle object that is passed onto onCreate
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_itineraries);

        Intent i = getIntent();
        fsm = (FlightServiceManager) i.getSerializableExtra(LoginActivity.FSM_KEY);
        user = (User) i.getSerializableExtra(LoginActivity.USER_DATA_KEY);
        fromClient = (boolean) i.getSerializableExtra("FROM_CLIENT");
    }

    /**
     * Searches through FlightServiceManager for the matching Itinerary.
     * @param view the interface component of this activity
     */
    public void searchItinerary(View view){
        EditText departureDateField = (EditText) findViewById(R.id.dateField);
        EditText originField = (EditText) findViewById(R.id.originField);
        EditText destinationField = (EditText) findViewById(R.id.destinationField);

        String date = departureDateField.getText().toString();
        date = date + " 00:00";
        String origin = originField.getText().toString();
        String destination = destinationField.getText().toString();

        this.itineraryList = fsm.getItinerary(origin, date, destination);
        ArrayList<String> stringItineraryList = new ArrayList<String>();

        // convert list of Flight object into list strings represents each Flight objects.
        for (Itinerary x: this.itineraryList){
            stringItineraryList.add(x.toString());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, stringItineraryList);
        ListView listView = (ListView) findViewById(R.id.flightField);
        listView.setAdapter(adapter);

        // tells what happen when clicked
        listView.setOnItemClickListener(mMessageClickedHandler);


        Toast search = Toast.makeText(getApplicationContext(), "Searching...", Toast.LENGTH_SHORT);
        search.show();
    }

    /**
     * An on click handler that pass on the position of what is been clicked.
     */
    private AdapterView.OnItemClickListener mMessageClickedHandler
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView adapter, View view, int position, long id) {
            // if the current user is an admin
            if (!fromClient){
                addItineraryToClient(position);
            }
            // if the current user is a client
            else {
                addItinerary(position);
            }
        }
    };

    /**
     * Sorts the Itinerary by time and display it.
     * @param view the interface component of this activity
     */
    public void sortItineraryByTime(View view){
        this.itineraryList = fsm.sortByTime(this.itineraryList);
        // use helper function setItineraryToView to set list of itineraries to ListView
        setItineraryToView(this.itineraryList);
    }

    /**
     * Sorts the Itinerary by cost and display it.
     * @param view the interface component of this activity
     */
    public void sortItineraryByCost(View view){
        this.itineraryList = fsm.sortByCost(this.itineraryList);
        // use helper function setItineraryToView to set list of itineraries to ListView
        setItineraryToView(this.itineraryList);
    }

    /**
     * Adds the Itinerary to the Client picked on click.
     * @param position the position of the Client been clicked on listView.
     */
    public void addItineraryToClient(int position){
        // save the selected Itinerary to be added
        this.selectedItinerary = this.itineraryList.get(position);
        // overwrite and display all the client on ListView
        ArrayList<User> userList = fsm.getUserList();
        ArrayList<String> clientStringList = new ArrayList<String>();
        this.clientList = new ArrayList<>();
        for (User x: userList){
            // distinguish admin and client, only display client
            if(x.getClass().equals(Client.class)){
                clientStringList.add(x.toString());
                this.clientList.add((Client)x);
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, clientStringList);
        ListView listView = (ListView) findViewById(R.id.flightField);
        listView.setAdapter(adapter);

        // tells it what happens if one user on ListView gets clicked
        listView.setOnItemClickListener(ItineraryHandler);

    }

    /**
     * An on click handler that pass on the position of what is been clicked.
     */
    private AdapterView.OnItemClickListener ItineraryHandler
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView adapter, View view, int position, long id) {
            addItinerarytoSelectedClient(position);
        }
    };

    /**
     * Adds the Itinerary to current Client.
     * @param position the position of Itinerary been clicked on listView.
     */
    public void addItinerary(int position){
        Itinerary i = this.itineraryList.get(position);
        ((Client) this.user).addItinerary(i);
        for (Flight f: i.getFlights()){
            f.book();
        }
        fsm.saveUserData();
        Toast successful = Toast.makeText(getApplicationContext(),
                "Your Itinerary (" + i.toString() +
                        ")has been successfully added",
                Toast.LENGTH_SHORT);
        successful.show();

        Intent intent = new Intent(this, ClientActivity.class);
        intent.putExtra(LoginActivity.FSM_KEY, fsm);
        intent.putExtra(LoginActivity.USER_DATA_KEY, user);
        startActivity(intent);
    }

    /**
     * Adds the Itinerary to the preselected Client.
     * @param position the position of Itinerary been clicked on listView
     */
    public void addItinerarytoSelectedClient(int position){
        Client c = this.clientList.get(position);
        c.addItinerary(this.selectedItinerary);
        for (Flight f: this.selectedItinerary.getFlights()){
            f.book();
        }
        fsm.saveUserData();
        Toast successful = Toast.makeText(getApplicationContext(),
                "Your Itinerary (" + selectedItinerary.toString() +
                        ")has been successfully added to User (" +
                        c.toString() + ")", Toast.LENGTH_SHORT);
        successful.show();

        Intent intent = new Intent(this, AdminActivity.class);
        intent.putExtra(LoginActivity.FSM_KEY, fsm);
        intent.putExtra(LoginActivity.USER_DATA_KEY, user);
        startActivity(intent);
    }

    /**
     * Sets the ArrayList of Itinerary to the listView.
     * @param itineraries ArrayList of Itinerary
     */
    private void setItineraryToView(ArrayList<Itinerary> itineraries){
        ArrayList<String> stringItineraryList = new ArrayList<String>();
        for (Itinerary x: itineraries){
            stringItineraryList.add(x.toString());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, stringItineraryList);
        ListView listView = (ListView) findViewById(R.id.flightField);
        listView.setAdapter(adapter);
    }

    /**
     * creates and sends an intent to the previous activity.
     * @param view
     */
    public void goBack(View view){
        if (fromClient) {
            Intent intent = new Intent(this, ClientActivity.class);
            intent.putExtra(LoginActivity.FSM_KEY, fsm);
            intent.putExtra(LoginActivity.USER_DATA_KEY, user);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, AdminActivity.class);
            intent.putExtra(LoginActivity.FSM_KEY, fsm);
            intent.putExtra(LoginActivity.USER_DATA_KEY, user);
            startActivity(intent);
        }
    }

}
