package com.example.albertxie.backend;

import java.util.ArrayList;
import java.util.List;

/**
 * Object that stores information of a single client.
 */
public class Client extends User {
	
	private static final long serialVersionUID = -2770034684335038251L;

	// Instance variables
	private String lastName;
	private String firstName;
	private String address;
	private String creditCardNumber;
	private String expiryDate;
	
	// booked itineraries is a list of itinerary objects
	private List<Itinerary> bookedItineraries = new ArrayList<Itinerary>();
	
	/**
	 * Initializes a new Client object with the given information.
	 * @param firstName	the first name of the client
	 * @param lastName  the last name of the client
	 * @param email     the email of the client
	 * @param address   the address of the client
	 * @param creditCardNumber	the credit card number of the client
	 * @param expiryDate the expiry date of client's credit card, stored 
	 * 					 in the format YYYY-MM-DD
	 * @param password	the password of the client
	 */
	public Client(String lastName, String firstName, String email,
			String address, String creditCardNumber, String expiryDate,
			String password) {
		
		super(password, email);
		this.lastName = lastName;
		this.firstName = firstName;
		this.address = address;
		this.creditCardNumber = creditCardNumber;
		this.expiryDate = expiryDate;
	}

	/**
	 * Returns the last name of Client.
	 * @return the lastName of Client
	 */
	public String getLastName() {
		return this.lastName;
	}
	
	/**
	 * Sets the Client with a new last name.
	 * @param lastName	the new lastName of the Client
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	/**
	 * Returns the first name of Client.
	 * @return the firstName of Client
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * Sets the Client with a new first name.
	 * @param firstName	the new firstName of the Client
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	/**
	 * Returns the address of Client.
	 * @return the address of Client
	 */
	public String getAddress() {
		return address;
	}
	
	/**
	 * Sets the Client with a new address.
	 * @param address 	the new address of the Client
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	
	/**
	 * Returns the credit card number of Client.
	 * @return the creditCardNumber of Client
	 */
	public String getCreditCardNumber() {
		return creditCardNumber;
	}
	
	/**
	 * Sets the Client with a new credit card number.
	 * @param creditCardNumber	the new credit card number of the Client
	 */
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}
	
	/**
	 * Returns the expiry date of Clients credit card.
	 * @return the expiryDate of Client
	 */
	public String getExpiryDate() {
		return expiryDate;
	}
	
	/**
	 * Sets the Client with a new expiry date for credit card number.
	 * @param expiryDate the new expiryDate of the Client
	 */
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	
	/**
	 * Adds itinerary to Client's list of booked itineraries.
	 * @param itinerary	new itinerary being added to bookedItineraries
	 */
	public void addItinerary(Itinerary itinerary) {
		// exist indicates if the given itinerary is already
		// inside this Client
		boolean exist = false;
		for (Itinerary i: this.bookedItineraries){
			if (i.equals(itinerary)){
				exist = true;
			}
		}
		// add chosen itinerary to clients list of booked itineraries
		if (!exist){
			this.bookedItineraries.add(itinerary);
		}
	}
	
	/**
	 * Returns the booked itineraries of Client.
	 * @return list of clients booked itineraries
	 */
	public List<Itinerary> getItineraries() {
		return this.bookedItineraries;
	}


	/**
	 * Returns the String representation of Client. 
	 * where the expiry date is stored in the format YYYY-MM-DD,.
	 * @return the String representation of Client.
	 */
	public String toString(){
		return (this.getLastName() + "," + this.getFirstName() + ","
				+ this.getEmail() + "," + this.getAddress() + ","
				+ this.getCreditCardNumber() + "," + this.getExpiryDate());
	}
}
