package com.example.albertxie.flightservice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.albertxie.backend.Admin;
import com.example.albertxie.backend.Client;
import com.example.albertxie.backend.Flight;
import com.example.albertxie.backend.FlightServiceManager;
import com.example.albertxie.backend.User;

import java.io.File;

/**
 * Activity that logs the User in.
 */
public class LoginActivity extends AppCompatActivity {

    private FlightServiceManager flightServiceManager;

    // files storing persistent data on the Android device
    public static final String USER_FILENAME = "user.ser";
    public static final String FLIGHT_FILENAME = "flight.ser";

    private static final String PASSWORDS = "passwords.txt";

    // directory containing persistent data files
    public static final String DATA_DIR = "fsmdata";


    public static final String FSM_KEY = "FSM_KEY";
    public static final String USER_DATA_KEY ="USER_DATA";


    /**
     * Initialises this activity.
     * @param savedInstanceState the Bundle object that is passed onto onCreate
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        File fsmdata = this.getApplicationContext()
                .getDir(DATA_DIR, MODE_PRIVATE);
        File userFile = new File(fsmdata, USER_FILENAME);
        File flightFile = new File(fsmdata, FLIGHT_FILENAME);

        File passwordsFile = new File(fsmdata, PASSWORDS);
        String passwordsPath = passwordsFile.getPath();


        //Loads the initial set of users from passwords.txt
        flightServiceManager = new FlightServiceManager(userFile, flightFile);
        flightServiceManager.uploadUserFromCSV(passwordsPath);
        flightServiceManager.saveUserData();
    }

    /**
     * Submits the login information of User, and moves to AdminActivity
     * if its an admin, if its a
     * client moves to ClientActivity.
     * @param view the interface component of this activity
     */
    public void submitLogin(View view) {

        EditText usernameField = (EditText) findViewById(R.id.usernameField);
        EditText passwordField = (EditText) findViewById(R.id.passwordField);

        String username = usernameField.getText().toString();
        String password = passwordField.getText().toString();

        User user = flightServiceManager.getUserByEmail(username);

        if((user != null) && validatePassword(user, password)) {

            Intent loginIntent;

            if(isAdmin(user)) {
                loginIntent = new Intent(this, AdminActivity.class);
                loginIntent.putExtra(FSM_KEY, flightServiceManager);
                loginIntent.putExtra(USER_DATA_KEY, user);
            }

            else {
                loginIntent = new Intent(this, ClientActivity.class);
                loginIntent.putExtra(FSM_KEY, flightServiceManager);
                loginIntent.putExtra(USER_DATA_KEY, user);
            }
            startActivity(loginIntent);
        }

        else {
            CharSequence msg = "Invalid login information!";
            Toast toast = Toast.makeText(
                    getApplicationContext(), msg, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    /**
     * Returns true if the user is an admin, false otherwise.
     * @param user the User object to be checked
     * @return true if the user is an admin, false otherwise
     */
    private boolean isAdmin(User user) {
        return user.getClass().equals(Admin.class);
    }

    /**
     * Returns true if the password entered is valid, false otherwise.
     * @param user the user
     * @param password the password of the user
     * @return true if the password entered is valid, false otherwise
     */
    private boolean validatePassword(User user, String password) {
        return user.getPassword().equals(password);
    }
}
