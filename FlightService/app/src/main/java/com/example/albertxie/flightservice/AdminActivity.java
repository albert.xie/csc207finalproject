package com.example.albertxie.flightservice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.albertxie.backend.FlightServiceManager;
import com.example.albertxie.backend.User;

/**
 * Initialize Main Activity that gives Admin access to the admins features
 */
public class AdminActivity extends AppCompatActivity {

    private FlightServiceManager fsm;
    private User user;

    @Override
    /**
     * Unpack intent and create the list view for menu selection.
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);


        Intent intent = getIntent();
        fsm = (FlightServiceManager) intent.getSerializableExtra
                (LoginActivity.FSM_KEY);
        user = (User) intent.getSerializableExtra(LoginActivity.USER_DATA_KEY);

        String[] activityFunctions = new String[]{
                "Search Flights",
                "Search Itineraries",
                "View Clients",
                "View Clients Itineraries",
                "Upload"
        };


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, activityFunctions);
        ListView listView = (ListView) findViewById(R.id.admin_options_list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(mMessageClickedHandler);
    }

    /**
     * When list view is clicked, run method based on selection made.
     */
    private AdapterView.OnItemClickListener mMessageClickedHandler =
            new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView parent, View view,
                                int position, long id) {
            switch(position) {

                case 0: searchFlights();
                    break;
                case 1: searchItineraries();
                    break;
                case 2: adminViewClients();
                    break;
                case 3: adminViewBooked();
                    break;
                case 4: adminUploadCSV();
                    break;
            }
        }
    };

    /**
     * Create a new intent to change the activity to
     * SearchFlightsActivity
     */
    private void searchFlights() {
        Intent intent = new Intent(this, SearchFlightsActivity.class);
        intent.putExtra(LoginActivity.FSM_KEY, fsm);
        intent.putExtra(LoginActivity.USER_DATA_KEY, user);
        startActivity(intent);
    }

    /**
     * Create a new intent to change the activity to
     * AdminViewBookedItineraries
     */
    private void adminViewBooked(){
        Intent intent = new Intent(this, AdminViewBookedItineraries.class);
        intent.putExtra(LoginActivity.FSM_KEY, fsm);
        intent.putExtra(LoginActivity.USER_DATA_KEY, user);
        startActivity(intent);
    }

    /**
     * Create a new intent to change the activity to
     * SearchItinerariesActivity
     */
    private void searchItineraries() {
        Intent intent = new Intent(this, SearchItinerariesActivity.class);
        intent.putExtra(LoginActivity.FSM_KEY, fsm);
        intent.putExtra(LoginActivity.USER_DATA_KEY, user);
        intent.putExtra("FROM_CLIENT", false);
        startActivity(intent);
    }

    /**
     * Create a new intent to change the activity to AdminViewClientsActivity
     */
    private void adminViewClients() {
        Intent intent = new Intent(this, AdminViewClientsActivity.class);
        intent.putExtra(LoginActivity.FSM_KEY, fsm);
        intent.putExtra(LoginActivity.USER_DATA_KEY, user);
        Log.d("debug trace 1", "starting admin view Activity");
        startActivity(intent);
    }

    /**
     * create a new intent change the activity to AdminUploadCSVActivity
     */
    private void adminUploadCSV() {
        Intent intent = new Intent(this, AdminUploadCSVActivity.class);
        intent.putExtra(LoginActivity.FSM_KEY, fsm);
        intent.putExtra(LoginActivity.USER_DATA_KEY, user);
        startActivity(intent);
    }
}
