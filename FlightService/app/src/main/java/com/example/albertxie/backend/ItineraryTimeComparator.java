package com.example.albertxie.backend;

import java.util.Comparator;

/**
 * ItineraryTimeComparator object that compares the time in Itinerary.
 */
public class ItineraryTimeComparator implements Comparator<Itinerary> {
	
	/**
	 * Returns an integer indicating which Itinerary object is greater.
	 * @param i1  the first Itinerary object to be compared
	 * @param i2  the second Itinerary object to be compared
	 * @return an integer indicating which Itinerary object is greater
	 */
	public int compare(Itinerary i1, Itinerary i2) {
		return (int) (i1.getTotalTime() - i2.getTotalTime());
	}
}
