package com.example.albertxie.backend;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Flight class object to store information about an individual flight.
 */
public class Flight implements Serializable {
	
	private static final long serialVersionUID = -199653462925087017L;

	// Instance variables
	private String flightNumber;
	private String departureDate;
	private String arrivalDate;
	private Date departureTime;
	private Date arrivalTime;
	private String airline;
	private String origin;
	private String destination;
	private double cost;
	private int seat;
	
	/**
	 * Instantiates a new Flight with specified flight information.
	 * @param flightNumber  the flight number of the flight
	 * @param departureTime the departure time of the flight, stored in the 
	 * 						format YYYY-MM-DD HH:MM
	 * @param arrivalTime   the arrival time of the flight, stored in the 
	 * 						format YYYY-MM-DD HH:MM
	 * @param airline 	    the airline of the flight
	 * @param origin 		the origin of the flight
	 * @param destination   the destination of the flight
	 * @param cost          the cost of the flight
	 * @param seat 			number of seats available
	 */
	public Flight(String flightNumber, String departureTime, 
			String arrivalTime, String airline, String origin, 
			String destination, double cost, int seat) {
		
		this.flightNumber = flightNumber;
		this.airline = airline;
		this.origin = origin;
		this.destination = destination;
		this.cost = cost;
		this.departureDate = departureTime;
		this.arrivalDate = arrivalTime;
		this.seat = seat;
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {
			this.arrivalTime = format.parse(arrivalTime);
			this.departureTime = format.parse(departureTime);
			
		} catch(ParseException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns the flight number of the flight.
	 * @return the flight number of the flight
	 */
	public String getFlightNumber(){
		return this.flightNumber;
	}
	
	/**
	 * Returns the departure time of the flight.
	 * @return the departure time of the flight 
	 */
	public Date getDepartureTime() {
		return this.departureTime;
	}
	
	/**
	 * Returns the arrival time of the flight.
	 * @return the arrival time of the flight
	 */
	public Date getArrivalTime(){
		return this.arrivalTime;
	}
	
	/**
	 * Returns the airline of the flight.
	 * @return the airline of the flight
	 */
	public String getAirline(){
		return this.airline;
	}
	
	/**
	 * Returns the origin of the flight.
	 * @return the origin of the flight
	 */
	public String getOrigin(){
		return this.origin;
	}
	
	/**
	 * Returns the destination of the flight.
	 * @return the destination of the flight
	 */
	public String getDestination(){
		return this.destination;
	}
	
	/**
	 * Returns the cost of the flight.
	 * @return the cost of the flight
	 */
	public double getCost(){
		return this.cost;
	}
	
	/**
	 * Returns the arrival date of the flight.
	 * @return the arrival date of the flight
	 */
	public String getArrivalDate() {
		return this.arrivalDate;
	}
	
	/**
	 * Returns the departure date of the flight.
	 * @return the departure date of the flight
	 */
	public String getDepartureDate() {
		return this.departureDate;
	}
	
	/**
	 * Returns the String representation of Flight, where the date and time
	 * is stored in the format YYYY-MM-DD HH:MM.
	 * @return the String representation of Flight
	 */
	public String toString(){
		Double cost = this.getCost();
		String strCost = String.format("%.2f", cost);
		String seats = Integer.toString(this.getSeat());
		
		return (this.getFlightNumber() + "," + this.departureDate + "," +
				this.arrivalDate + "," + this.getAirline() + "," +
				this.getOrigin() +
				"," + this.getDestination() + "," + strCost + ", " +
		    	this.convertTime(
						this.getArrivalTime().getTime() -
								this.getDepartureTime().getTime()) +
			", " + seats);
	}

	/**
	 * Returns the totalTime in millisecond to the format HH:MM.
	 * @param totalTime time in milliseconds
	 * @return the totalTime in millisecond to the format HH:MM
	 */
	private String convertTime(long totalTime) {
		int minutes = (int) (totalTime /
				((int) Constants.SECONDS_IN_MINUTE *
						Constants.MILLISECONDS_IN_SECOND));
		String hours = Integer.toString((int) (minutes /
				(int) Constants.MINUTES_IN_HOUR));
		String remainingMinutes = Integer.toString(minutes %
				(int) Constants.MINUTES_IN_HOUR);

		if(hours.length() <= 1) {
			hours = "0" + hours;
		}
		if(remainingMinutes.length() <= 1) {
			remainingMinutes = "0" + remainingMinutes;
		}
		return hours + ":" + remainingMinutes;
	}

	/**
	 * Edits the Flight object with parameters given, does not modify the
	 * fields that parameters
	 * wasn't given.
	 * @param flightNumber	the flight number of the flight
	 * @param departureTime	the departure time of the flight
	 * @param arrivalTime	the arrival time of the flight
	 * @param airline		the airline of the flight
	 * @param origin		the origin of the flight
	 * @param destination	the destination of the flight
	 * @param cost			the cost of the flight
	 */
	public void editFlight(String flightNumber, String departureTime,
						   String arrivalTime, String airline, String origin,
						   String destination, double cost) {

		this.flightNumber = flightNumber;
		this.airline = airline;
		this.origin = origin;
		this.destination = destination;
		this.cost = cost;

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		// change departure and arrival time only when given
		// parameter not empty;
		if (!(departureTime.equals(""))){
			try {
				this.departureTime = format.parse(departureTime);

			} catch (ParseException e) {
				e.printStackTrace();
			}
		} if (!(arrivalTime.equals(""))){
			try {
				this.arrivalTime = format.parse(arrivalTime);

			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Returns true if other is a Flight object and has the same
	 * flight number as the flight,
	 * false otherwise.
	 * @param other the object it is been compared with.
	 * @return true if other is a Flight object and has the same
	 * flight number as the flight,
	 * false otherwise
	 */
	public boolean equals(Object other){
		// if other is a FLight object
		if (other.getClass().equals(this.getClass())){
			// if they have the same flight number indicating
			// they are the same flight
			if (this.getFlightNumber().equals(((Flight)other)
					.getFlightNumber())){
				return true;
			}
		} return false;
	}

	/**
	 * Returns true if the Flight is not full, false otherwise.
	 * @return true if the Flight is not full, flase otherwise
	 */
	public boolean haveSpace(){
		if (this.seat <= 0){
			return false;
		} return true;
	}

	/**
	 * Reduce the amount of seats by one when ever this Flight is booked.
	 */
	public void book() {
		this.seat = this.seat - 1;
	}

	/**
	 * Returns the amount of seats this FLight has.
	 * @return the amount of seats this FLight has
	 */
	public int getSeat() {
		return this.seat;
	}
}
