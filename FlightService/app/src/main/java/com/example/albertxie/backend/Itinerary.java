package com.example.albertxie.backend;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * Itinerary object that stores all the Flight object in the itinerary.
 */
public class Itinerary implements Serializable{

	private static final long serialVersionUID = -8672083530170232053L;
	
	private LinkedList<Flight> flights;
	private double totalCost;
	private long totalTime;
	
	/**
	 * Initialize the Itinerary.
	 */
	public Itinerary() {
		this.flights = new LinkedList<Flight>();
		this.totalCost = 0;
		this.totalTime = 0;
	}

	/**
	 * Adds a new Flight object to the Itinerary.
	 * @param flight Flight object to be added to Itinerary
	 */
	public void addFlight(Flight flight) {
		this.flights.add(flight);
		this.totalCost += flight.getCost();
		this.totalTime = this.flights.getLast().getArrivalTime().getTime()
				- this.flights.getFirst().getDepartureTime().getTime();
	}

	/**
	 * Returns the total cost of the Flight objects inside the Itinerary.
	 * @return the total cost of the Flight objects inside the Itinerary
	 */
	public double getTotalCost(){
		return this.totalCost;
	}
	
	/**
	 * Returns the total time in minutes of the Flight objects.
	 * @return the total time in minutes of the Flight objects.
	 */
	public long getTotalTime(){
		return this.totalTime;
	}
	
	/**
	 * Returns the last Flight object stored in this Itinerary.
	 * @return the last Flight object stored in this Itinerary
	 */
	public Flight getLast(){
		return this.flights.getLast();
	}
	
	/**
	 * Returns the whole LinkedList of Flight objects thats been stored.
	 * @return LinkedList of Flight objects
	 */
	public LinkedList<Flight> getFlights(){
		return this.flights;
	}
	
	/**
	 * Converts totalTime (in milliseconds) to a HH:MM format
	 * @param totalTime in milliseconds
	 * @return string
	 */
	private String convertTime(long totalTime) {
		int minutes = (int) (totalTime / 
				((int) Constants.SECONDS_IN_MINUTE * 
						Constants.MILLISECONDS_IN_SECOND));
		String hours = Integer.toString((int) (minutes / 
				(int) Constants.MINUTES_IN_HOUR));
		String remainingMinutes = Integer.toString(minutes % 
				(int) Constants.MINUTES_IN_HOUR);
		
		if(hours.length() <= 1) {
			hours = "0" + hours;
		}
		if(remainingMinutes.length() <= 1) {
			remainingMinutes = "0" + remainingMinutes;
		}
		return hours + ":" + remainingMinutes;
	}

	/**
	 * Returns the String representation of this Itinerary.
	 * @return string
	 */
	public String toString() {
		String returnVal = "";
		for(Flight f : this.getFlights()) {
			returnVal += f.getFlightNumber() + "," + f.getDepartureDate()+ ","
					     + f.getArrivalDate() + "," + f.getAirline() + "," 
					     + f.getOrigin() + "," + f.getDestination() + ","
						 + Integer.toString(f.getSeat()) + "\n";
		} 
		String strCost = String.format("%.2f", this.totalCost);
		return returnVal + strCost + "\n" + this.convertTime(this.totalTime);
	}

	/**
	 * Returns true if this Itinerary has the same Flight
	 * in the same order as other.
	 * @param other object that is been compared.
	 * @return true if this Itinerary has the same Flight
	 * in the same order as other
	 */
	public boolean equals(Object other){
		// flag shows if there is one Flight that does not match
		boolean flag = true;
		// if other is a Itinerary
		if (other.getClass().equals(this.getClass())){
			Itinerary otherItinerary = (Itinerary)other;
			// if other has the same length
			if (this.flights.size() == (otherItinerary.flights.size())){
				for (int i = 0; i > this.flights.size(); i++) {
					if (!this.flights.get(i)
							.equals(otherItinerary.flights.get(i))) {
						flag = false;
					}
				}
			} return flag;
		} return false;
	}
}
