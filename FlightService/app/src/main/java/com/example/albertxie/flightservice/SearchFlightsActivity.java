package com.example.albertxie.flightservice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.albertxie.backend.Admin;
import com.example.albertxie.backend.Flight;
import com.example.albertxie.backend.FlightServiceManager;
import com.example.albertxie.backend.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity that searches for Flight based on input.
 */
public class SearchFlightsActivity extends AppCompatActivity {

    FlightServiceManager fsm;
    User user;
    List<Flight> flightList;

    /**
     * Initialises this activity.
     * @param savedInstanceState the Bundle object that is passed onto onCreate
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_flights);

        Intent i = getIntent();
        fsm = (FlightServiceManager)
                i.getSerializableExtra(LoginActivity.FSM_KEY);
        user = (User) i.getSerializableExtra(LoginActivity.USER_DATA_KEY);
    }


    /**
     * Searches for the matching FLight based on input.
     * @param view the interface component of this activity
     */
    public void searchFlight(View view){
        EditText departureDateField = (EditText) findViewById(R.id.dateField);
        EditText originField = (EditText) findViewById(R.id.originField);
        EditText destinationField = (EditText)
                findViewById(R.id.destinationField);

        String date = departureDateField.getText().toString();
//        date = date + " 00:00";
        String origin = originField.getText().toString();
        String destination = destinationField.getText().toString();

        this.flightList = fsm.searchFlight(date, origin, destination);
        ArrayList<String> stringFlightList = new ArrayList<String>();

        // convert list of Flight object into list
        // strings represents each Flight objects.
        for (Flight x: this.flightList){
            stringFlightList.add(x.toString());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, stringFlightList);
        ListView listView = (ListView) findViewById(R.id.flightField);
        listView.setAdapter(adapter);

        // tells what happen when clicked
        listView.setOnItemClickListener(mMessageClickedHandler);


        Toast search = Toast.makeText(getApplicationContext(),
                "Searching...", Toast.LENGTH_SHORT);
        search.show();
    }

    /**
     * An on click handler that pass on the position of what is been clicked.
     */
    private AdapterView.OnItemClickListener mMessageClickedHandler
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView adapter, View view, int position, long id) {
            // if the current user is an admin
            if (user.getClass().equals(Admin.class)) {
                editFlight(position);
            } else {
                Toast message = Toast.makeText(getApplicationContext(),
                        "You must be an Admin to edit Flight",
                        Toast.LENGTH_SHORT);
                message.show();
            }
        }
    };

    /**
     * Edits the Flight based on input.
     * @param position the position of the Flight that is been clicked
     */
    private void editFlight(int position){
        Intent intent = new Intent(this, AdminEditFlightActivity.class);
        intent.putExtra(LoginActivity.FSM_KEY, fsm);
        Flight flight = this.flightList.get(position);
        intent.putExtra("FLIGHT_KEY", flight);
        startActivity(intent);
    }
}