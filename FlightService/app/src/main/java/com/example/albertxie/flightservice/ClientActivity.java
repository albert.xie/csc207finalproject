package com.example.albertxie.flightservice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.albertxie.backend.FlightServiceManager;
import com.example.albertxie.backend.User;

/**
 * Activity that shows the available option for Client.
 */
public class ClientActivity extends AppCompatActivity {

    private FlightServiceManager fsm;
    private User user;

    /**
     * Initialises this activity.
     * @param savedInstanceState the Bundle object that is passed onto onCreate
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);

        Intent i = getIntent();
        fsm = (FlightServiceManager)
                i.getSerializableExtra(LoginActivity.FSM_KEY);
        user = (User) i.getSerializableExtra(LoginActivity.USER_DATA_KEY);

        String[] activityFunctions = new String[]{
                "Search Flight",
                "Search Itineraries",
                "Update Personal Info",
                "View Your Itineraries"
        };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, activityFunctions);
        ListView listView = (ListView) findViewById(R.id.client_options_list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(mMessageClickedHandler);
    }

    /**
     * An on click handler that pass on the position of what is been clicked.
     */
    private AdapterView.OnItemClickListener mMessageClickedHandler
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            switch (position) {
                case 0:
                    searchFlight();
                    break;
                case 1:
                    searchItineraries();
                    break;
                case 2:
                    clientUpdateInfo();
                    break;
                case 3:
                    clientViewBookedItineraries();
                    break;
            }
        }
    };

    /**
     * Goes to SearchFlightsActivity.
     */
    private void searchFlight() {
        Intent intent = new Intent(this, SearchFlightsActivity.class);
        intent.putExtra(LoginActivity.FSM_KEY, fsm);
        intent.putExtra(LoginActivity.USER_DATA_KEY, user);
        startActivity(intent);
    }

    /**
     * Goes to SearchItinerariesActivity.
     */
    private void searchItineraries() {
        Intent intent = new Intent(this, SearchItinerariesActivity.class);
        intent.putExtra(LoginActivity.FSM_KEY, fsm);
        intent.putExtra(LoginActivity.USER_DATA_KEY, user);
        intent.putExtra("FROM_CLIENT", true);
        startActivity(intent);
    }

    /**
     * Goes to ClientViewBookedItineraries.
     */
    private void clientViewBookedItineraries() {
        Intent intent = new Intent(this, ClientViewBookedItineraries.class);
        intent.putExtra(LoginActivity.FSM_KEY, fsm);
        intent.putExtra(LoginActivity.USER_DATA_KEY, user);
        intent.putExtra("FROM_CLIENT", true);
        startActivity(intent);
    }

    /**
     * Goes to UpdateUserInfoActivity.
     */
    private void clientUpdateInfo() {
        Intent intent = new Intent(this, UpdateUserInfoActivity.class);
        intent.putExtra(LoginActivity.FSM_KEY, fsm);
        intent.putExtra(LoginActivity.USER_DATA_KEY, user);
        intent.putExtra("FROM_CLIENT", true);
        startActivity(intent);
    }
}
