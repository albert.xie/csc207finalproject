package com.example.albertxie.flightservice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.albertxie.backend.Admin;
import com.example.albertxie.backend.Client;
import com.example.albertxie.backend.FlightServiceManager;

/**
 * Activity that edits the User information.
 */
public class UpdateUserInfoActivity extends AppCompatActivity {

    private FlightServiceManager fsm;
    private Client client;
    private Admin admin;
    private boolean fromClient;

    private Client clientPointer;

    /**
     * Initialises this activity.
     * @param savedInstanceState the Bundle object that is passed onto onCreate
     */
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_user_info);

        Intent i = getIntent();

        fsm = (FlightServiceManager)
                i.getSerializableExtra(LoginActivity.FSM_KEY);
        fromClient = (boolean) i.getSerializableExtra("FROM_CLIENT");

        client = (Client) i.getSerializableExtra(LoginActivity.USER_DATA_KEY);

        clientPointer = (Client) fsm.getUserByEmail(client.getEmail());

        if (!fromClient) {
            admin = (Admin) i.getSerializableExtra("ADMIN_DATA");
        }

        EditText editLastName = (EditText)findViewById(R.id.changeLastName);
        editLastName.setText(client.getLastName(),
                TextView.BufferType.EDITABLE);

        EditText editFirstName = (EditText)findViewById(R.id.changeFirstName);
        editFirstName.setText(client.getFirstName(),
                TextView.BufferType.EDITABLE);

        EditText editAddress = (EditText)findViewById(R.id.changeAddress);
        editAddress.setText(client.getAddress(),
                TextView.BufferType.EDITABLE);

        EditText editCreditCard = (EditText)
                findViewById(R.id.changeCreditCardNumber);
        editCreditCard.setText(client.getCreditCardNumber(),
                TextView.BufferType.EDITABLE);

        EditText editExpiryDate = (EditText)
                findViewById(R.id.changeExpiryDate);
        editExpiryDate.setText(client.getExpiryDate(),
                TextView.BufferType.EDITABLE);
    }

    /**
     * Update User information based on the input given.
     * @param view the interface component of this activity
     */
    public void makeChanges(View view) {
        EditText lastNameField = (EditText) findViewById(R.id.changeLastName);
        String lastName = lastNameField.getText().toString();

        if (!lastName.equals("")) {
            clientPointer.setLastName(lastName);
        }

        EditText firstNameField = (EditText)
                findViewById(R.id.changeFirstName);
        String firstName = firstNameField.getText().toString();
        if (!firstName.equals("")) {
            clientPointer.setFirstName(firstName);
        }

        EditText addressField = (EditText) findViewById(R.id.changeAddress);
        String address = addressField.getText().toString();
        if (!address.equals("")) {
            clientPointer.setAddress(address);
        }

        EditText creditCardField = (EditText)
                findViewById(R.id.changeCreditCardNumber);
        String creditCard = creditCardField.getText().toString();
        if (!creditCard.equals("")) {
            clientPointer.setCreditCardNumber(creditCard);
        }

        EditText expiryDateField = (EditText)
                findViewById(R.id.changeExpiryDate);
        String expiryDate = expiryDateField.getText().toString();
        if (!expiryDate.equals("")) {
            clientPointer.setExpiryDate(expiryDate);
        }

        fsm.saveUserData();

        if (fromClient) {
            Intent intent = new Intent(this, ClientActivity.class);
            intent.putExtra(LoginActivity.FSM_KEY, fsm);

            System.out.println(client);
            System.out.println(clientPointer);

            intent.putExtra(LoginActivity.USER_DATA_KEY, client);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this,
                    AdminViewClientsActivity.class);
            intent.putExtra(LoginActivity.FSM_KEY, fsm);

            System.out.println(client);
            System.out.println(clientPointer);

            intent.putExtra(LoginActivity.USER_DATA_KEY, admin);
            startActivity(intent);
        }
    }
}
