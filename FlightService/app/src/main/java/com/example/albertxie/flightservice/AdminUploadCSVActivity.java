package com.example.albertxie.flightservice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.albertxie.backend.Admin;
import com.example.albertxie.backend.Flight;
import com.example.albertxie.backend.FlightServiceManager;
import com.example.albertxie.backend.User;

import java.io.File;

/**
 * Activity used to upload flight information and user information from a file
 */
public class AdminUploadCSVActivity extends AppCompatActivity {

    private FlightServiceManager fsm;
    private User user;

    private final String DEFAULT_FLIGHTS_FILE = "flightsData.txt";
    private final String DEFAULT_USERS_FILE = "passwords.txt";

    private String defaultPathFlights, defaultPathUsers;

    @Override
    /**
     * Initialize activity and unpack the intent, setup EditText's for giving
     * location of files to be uploaded
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_upload_csv);

        Intent intent = getIntent();
        fsm = (FlightServiceManager) intent.getSerializableExtra
                (LoginActivity.FSM_KEY);
        user = (User) intent.getSerializableExtra(LoginActivity
                .USER_DATA_KEY);

        File fsmdata = this.getApplicationContext()
                .getDir(LoginActivity.DATA_DIR, MODE_PRIVATE);
        File flightsFile = new File(fsmdata, DEFAULT_FLIGHTS_FILE);
        File usersFile = new File(fsmdata, DEFAULT_USERS_FILE);


        defaultPathUsers = usersFile.getPath();
        defaultPathFlights = flightsFile.getPath();

        EditText flightsPathField = (EditText) findViewById
                (R.id.admin_upload_csv_path_field_flights);
        flightsPathField.setText(defaultPathFlights,
                TextView.BufferType.EDITABLE);

        EditText usersPathField = (EditText) findViewById
                (R.id.admin_upload_csv_path_field_users);
        usersPathField.setText(defaultPathUsers, TextView.BufferType.EDITABLE);
    }

    /**
     * Upload users from a new file, save userData and return to AdminACtivity
     * @param view default parameter
     */
    public void uploadUsersCSV(View view) {
        fsm.uploadUserFromCSV(defaultPathUsers);
        fsm.saveFlightData();

        Intent intent = new Intent(this, AdminActivity.class);

        intent.putExtra(LoginActivity.FSM_KEY, fsm);
        intent.putExtra(LoginActivity.USER_DATA_KEY, user);

        CharSequence msg = "Users uploaded!";
        Toast toast = Toast.makeText(getApplicationContext(),
                msg, Toast.LENGTH_SHORT);
        toast.show();

        startActivity(intent);
    }

    /**
     * Upload flight information from a file, save flight data and return to
     * AdminActivity
     * @param view default parameter
     */
    public void uploadFlightsCSV(View view) {

        fsm.uploadFlightFromCSV(defaultPathFlights);
        fsm.saveFlightData();

        Intent intent = new Intent(this, AdminActivity.class);

        intent.putExtra(LoginActivity.FSM_KEY, fsm);
        intent.putExtra(LoginActivity.USER_DATA_KEY, user);

        CharSequence msg = "Flights uploaded!";
        Toast toast = Toast.makeText(getApplicationContext(),
                msg, Toast.LENGTH_SHORT);
        toast.show();

        startActivity(intent);
    }
}
