package com.example.albertxie.backend;

/**
 * Class that represents various constants used in FlightService.Backend
 */
public class Constants {

	public static final int TIME_FRAME_HOURS = 6;
	
	public static final long MILLISECONDS_IN_SECOND = 1000;
	
	public static final long SECONDS_IN_MINUTE = 60;
	
	public static final long MINUTES_IN_HOUR = 60;
	
	public static final long MILLISECONDS_IN_HOUR =
			MILLISECONDS_IN_SECOND * SECONDS_IN_MINUTE * MINUTES_IN_HOUR;
	
}
