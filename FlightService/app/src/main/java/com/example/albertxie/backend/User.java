package com.example.albertxie.backend;

import java.io.Serializable;

/**
 * User object that stores information of a single user.
 */
public abstract class User implements Serializable {

	protected static final long serialVersionUID = -473967435264491485L;

	private String password;
	private String email;

	/**
	 * Initialize a new User object with the given information.
	 * @param email	the email of the User
	 * @param password	the password of the User
	 */
	public User(String password, String email) {
		this.password = password;
		this.email = email;
	}


	public String getPassword() {
		return password;
	}

	/**
	 * Sets the User with a new password.
	 * @param password	 the new password of the User
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * Returns the email of User.
	 * @return the email of User
	 */
	public String getEmail() {
		return email;
	}

	/**
	 *
	 * @param other
	 * @return
	 */
	public boolean equals(User other) {
		return this.email.equals(other.getEmail());
	}
}
