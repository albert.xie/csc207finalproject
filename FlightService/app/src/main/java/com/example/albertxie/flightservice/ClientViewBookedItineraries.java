package com.example.albertxie.flightservice;

import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.albertxie.backend.Admin;
import com.example.albertxie.backend.Client;
import com.example.albertxie.backend.FlightServiceManager;
import com.example.albertxie.backend.Itinerary;
import com.example.albertxie.backend.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity that views the booked Itinerary of Client.
 */
public class ClientViewBookedItineraries extends AppCompatActivity {

    private Client client;
    private FlightServiceManager fsm;
    private boolean fromClient;
    private Admin admin;
    public static final String FSM_KEY = "FSM_KEY";
    public static final String USER_DATA_KEY ="USER_DATA";

    /**
     * Initialises this activity, displayed the itineraries
     * that booked for this Client.
     * @param savedInstanceState the Bundle object that is passed onto onCreate
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_view_booked_itineraries);

        Intent intent = getIntent();
        fsm = (FlightServiceManager)
                intent.getSerializableExtra(LoginActivity.FSM_KEY);
        client = (Client)
                intent.getSerializableExtra(LoginActivity.USER_DATA_KEY);

        fromClient = (boolean) intent.getSerializableExtra("FROM_CLIENT");

        if (!fromClient) {
            admin = (Admin) intent.getSerializableExtra("ADMIN_DATA");
        }

        String[] strList = new String[client.getItineraries().size()];
        List<Itinerary> list = client.getItineraries();
        //for (Itinerary x: client.getItineraries()){
        //    list.add(x);
        //}
        int n = 0;
        while(n < strList.length){
            strList[n] = list.get(n).toString();
            n += 1;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, strList);
        ListView listView = (ListView) findViewById(R.id.itineraries_list);
        listView.setAdapter(adapter);
    }

    /**
     * Goes back to the previous Activity.
     * @param view the interface component of this activity
     */
    public void goBack(View view){
        if (fromClient) {
            Intent intent = new Intent(this, ClientActivity.class);
            intent.putExtra(LoginActivity.FSM_KEY, fsm);
            intent.putExtra(LoginActivity.USER_DATA_KEY, client);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, AdminActivity.class);
            intent.putExtra(LoginActivity.FSM_KEY, fsm);
            intent.putExtra(LoginActivity.USER_DATA_KEY, admin);
            startActivity(intent);
        }
    }
}
